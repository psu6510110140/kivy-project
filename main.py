from kivy.app import App 
from kivy.uix.screenmanager import Screen
from kivy.core.window import Window
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty

# this will be our winodw size for this tut
Window.size = (1200,400)

class Gameplay(Screen):
    # ps stands for player size
    ps = NumericProperty(77)

    # ww stands for window width
    ww = NumericProperty(1200)

    # wh stands for window height
    wh = NumericProperty(400)



class Wall(Widget):
    pass

class PacmanApp(App):
    def build(self):
        return Gameplay()

PacmanApp().run()